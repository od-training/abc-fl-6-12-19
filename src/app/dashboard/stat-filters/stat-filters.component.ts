import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent implements OnInit {
  filters: FormGroup;

  constructor(fb: FormBuilder) {
    this.filters = fb.group({
      region: [''],
      age: []
    });

    this.filters = new FormGroup({
      region: new FormControl(''),
      age: new FormControl()
    });

    (this.filters.get('region') as FormControl).valueChanges
      .subscribe(data => console.log(data));
  }

  ngOnInit() {
  }

  submitForm() {
    console.log(this.filters.value);
  }

}
