import { Component } from '@angular/core';

import { Video } from '@types';
import { VideoLoaderService } from '@services/video-loader.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent {
  videoList: Video[] = [];
  currentVideo: Video | undefined;

  constructor(vls: VideoLoaderService) {
    vls
      .loadVideos()
      .subscribe(videos => this.videoList = videos);
  }

  selectTheVideo(video: Video) {
    if (video === this.currentVideo) {
      this.currentVideo = undefined;
    } else {
      this.currentVideo = video;
    }
  }
}
